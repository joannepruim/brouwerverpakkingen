import { Component } from '@angular/core';

@Component({
  selector: "app-root",
  template: `      
    <router-outlet></router-outlet>
<!--    <nav>-->
<!--      <ul>-->
<!--        <li *ngFor="let link of links">-->
<!--          <a [routerLink]="link.url">{{ link.text }}</a>-->
<!--        </li>-->
<!--      </ul>-->
<!--    </nav>-->
  `,
  styles: [],
})
export class AppComponent {
  links = [
    { url: "", text: "Home" },
    { url: "about", text: "About" },
  ];
}
