import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { MenuModule } from "../menu/menu.module";
import { LogoModule } from "../logo/logo.module";
import { IntroModule } from "../intro/intro.module";
import {DienstenModule} from "../diensten/diensten.module";
import {ContactModule} from "../contact/contact.module";
import {FooterModule} from "../footer/footer.module";
import {AboutModule} from "../about/about.module";
import {ImagesModule} from "../images/images.module";

@NgModule({
  declarations: [
    HomeComponent
  ],
  exports: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MenuModule,
    LogoModule,
    IntroModule,
    DienstenModule,
    ContactModule,
    FooterModule,
    AboutModule,
    ImagesModule,
  ]
})
export class HomeModule { }
