import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DienstenRoutingModule } from './diensten-routing.module';
import { DienstenComponent } from './diensten.component';


@NgModule({
  declarations: [
    DienstenComponent
  ],
  exports: [
    DienstenComponent
  ],
  imports: [
    CommonModule,
    DienstenRoutingModule
  ]
})
export class DienstenModule { }
