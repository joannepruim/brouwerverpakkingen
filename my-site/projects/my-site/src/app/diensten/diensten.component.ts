import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diensten',
  templateUrl: './diensten.component.html',
  styleUrls: ['./diensten.component.styl']
})
export class DienstenComponent implements OnInit {

  constructor() { }

  floorsfirsttop = [
    { url: "verpakkingen", img: './assets/diensten/verpakkingentest.jpg', text: "verpakkingen" },
    { url: "slitting", img: './assets/diensten/slittingtest.jpg', text: "slitting" },
  ];

  ngOnInit(): void {
  }

}
