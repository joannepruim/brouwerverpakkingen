import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DienstenComponent } from './diensten.component';

const routes: Routes = [{ path: '', component: DienstenComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DienstenRoutingModule { }
