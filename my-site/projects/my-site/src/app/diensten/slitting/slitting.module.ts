import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SlittingRoutingModule } from './slitting-routing.module';
import { SlittingComponent } from './slitting.component';
import {MenuModule} from "../../menu/menu.module";
import {LogoModule} from "../../logo/logo.module";


@NgModule({
  declarations: [
    SlittingComponent
  ],
  exports: [
    SlittingComponent
  ],
  imports: [
    CommonModule,
    SlittingRoutingModule,
    MenuModule,
    LogoModule
  ]
})
export class SlittingModule { }
