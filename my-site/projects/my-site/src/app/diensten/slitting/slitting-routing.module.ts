import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SlittingComponent } from './slitting.component';

const routes: Routes = [{ path: '', component: SlittingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SlittingRoutingModule { }
