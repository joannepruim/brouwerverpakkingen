import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerpakkingenComponent } from './verpakkingen.component';

const routes: Routes = [{ path: '', component: VerpakkingenComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerpakkingenRoutingModule { }
