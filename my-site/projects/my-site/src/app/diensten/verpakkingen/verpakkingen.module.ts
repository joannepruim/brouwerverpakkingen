import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerpakkingenRoutingModule } from './verpakkingen-routing.module';
import { VerpakkingenComponent } from './verpakkingen.component';
import {MenuModule} from "../../menu/menu.module";
import {LogoModule} from "../../logo/logo.module";


@NgModule({
  declarations: [
    VerpakkingenComponent
  ],
  exports: [
    VerpakkingenComponent
  ],
  imports: [
    CommonModule,
    VerpakkingenRoutingModule,
    MenuModule,
    LogoModule
  ]
})
export class VerpakkingenModule { }
