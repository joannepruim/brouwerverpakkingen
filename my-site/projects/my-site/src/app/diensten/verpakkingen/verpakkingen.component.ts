import {Component, Inject, OnInit, PLATFORM_ID, ViewChild} from '@angular/core';
import {isPlatformBrowser} from "@angular/common";

@Component({
  selector: 'app-verpakkingen',
  templateUrl: './verpakkingen.component.html',
  styleUrls: ['./verpakkingen.component.styl']
})
export class VerpakkingenComponent implements OnInit {
  @ViewChild('controls', { static: true }) controls: any;
  @ViewChild('slidercontainer', { static: true }) public slider: any;

  // @ts-ignore
  private interval: NodeJS.Timeout;
  public currentIndex = 0;
  public quotes = [
    {
      image: './assets/review/verpakkingen1.jpg',
    },
    {
      image: './assets/review/verpakkingen3.jpg',
    },
    {
      image: './assets/review/verpakkingen2.jpg',
    },
    {
      image: './assets/review/verpakkingen4.jpg',
    },
  ];

  constructor(@Inject(PLATFORM_ID) private platform: any) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platform)) {
      this.interval = setInterval(() => {
        this.nextSlide();
      }, 6000);
    }
  }

  get maxItems() {
    return this.quotes.length - 1;
  }

  prevSlide() {
    let index = this.currentIndex - 1;
    if (index < 0) {
      index = this.maxItems;
    }
    this.currentIndex = index;
  }

  nextSlide() {
    let index = this.currentIndex + 1;
    if (index > this.maxItems) {
      index = 0;
    }
    this.currentIndex = index;
  }
}
