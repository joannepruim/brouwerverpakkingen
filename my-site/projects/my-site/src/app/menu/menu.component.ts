import { ViewportScroller } from '@angular/common';
import {MatSidenavModule} from '@angular/material/sidenav';
import { Component, OnInit, HostBinding, HostListener } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.styl']
})
export class MenuComponent {

    public isDisplay = false;
    toggleDisplay() {
      this.isDisplay = !this.isDisplay;
    }
    closeDisplay() {
      this.isDisplay = false;
    }
  public isOpen = false;

  private listenToDocumentClick = false;

  constructor(
      private viewportScroller: ViewportScroller) {
  }

  scrollTo(item: any) {
    this.viewportScroller.scrollToAnchor(item);
  }

  @HostBinding('class')
  get classList() {
    return 'lib-header-dropdown';
  }

  public onClick(): void {
    if (!this.isOpen) {
      setTimeout(() => {
        this.isOpen = true;
        this.listenToDocumentClick = true;
      });
    }
  }

  @HostListener('document:click')
  public onDocumentClick() {
    if (this.isOpen) {
      this.isOpen = false;
      this.listenToDocumentClick = false;
    }
  }
}
