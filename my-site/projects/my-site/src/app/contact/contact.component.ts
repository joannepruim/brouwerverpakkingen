import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from "@angular/common/http";

interface SubmitEvent extends Event {
  submitter: HTMLElement;
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.styl']
})
export class ContactComponent implements OnInit {
  isSuccess?: boolean;
  successMessage?: Object;
  errorMessage?: any;
  response?: Object;

  contactForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    phoneNumber: new FormControl('',[
      Validators.required,
      Validators.minLength(10),
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.pattern("[^ @]*@[^ @]*")
    ]),
    question: new FormControl('', Validators.required),
  });


  // endpoint = "https://formcarry.com/s/l_gXXt83oSf";
  // errorMessage?: string;
  // response?: Object;
  //
  // contactForm = new FormGroup({
  //   firstName: new FormControl('', Validators.required),
  //   lastName: new FormControl('', Validators.required),
  //   phoneNumber: new FormControl('',[
  //     Validators.required,
  //     Validators.minLength(10),
  //     Validators.maxLength(10)
  //   ]),
  //   email: new FormControl('', [
  //     Validators.required,
  //     Validators.pattern("[^ @]*@[^ @]*")
  //   ]),
  //   question: new FormControl('', Validators.required),
  // });

  constructor(private readonly http: HttpClient) { }

  ngOnInit(): void {
  }

  onSubmit(e: SubmitEvent) {
    // Prevent default to avoid sending the original HTML form (default browser action)
    e.preventDefault();

    // TODO: add some validation and prevent sending the form if the form still has errors
    if (this.contactForm.invalid) {
      this.errorMessage = 'fout'
    }
    if (this.contactForm.valid) {
      this.successMessage = 'lekker'
    }

    // Send the form
    this.http
        .post("https://formcarry.com/s/0c1QxD9Z3HU", this.contactForm.value, {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
          }
        })
        .subscribe({
          next: (res) => {
            this.response = res;
            this.isSuccess = true;
            this.successMessage = "Verzonden!";
            this.contactForm.reset();
          },
          error: (e) => {
            this.errorMessage = e;
          }
        });
  }

  // onSubmit(e: SubmitEvent) {
  //   e.preventDefault();
  //   if (this.contactForm.invalid) return;
  //   this.sendForm(this.endpoint, this.contactForm.value).subscribe({
  //     next: (res) => (this.response = res),
  //     error: (e) => (this.errorMessage = e)
  //   });
  //
  //   if (this.contactForm.valid) {
  //     this.contactForm.reset();
  //   }
  // }
  //
  // sendForm(endpoint: string, payload: ContactRequest) {
  //   return this.http.post(endpoint, payload, {
  //     headers: {
  //       "Content-Type": "application/json",
  //       Accept: "application/json"
  //     }
  //   });
  // }

  // onSubmitt() {
  //   // @ts-ignore
  //   console.log(this.formGroup.get('firstName').value);
  //
  //   if (this.contactForm.valid) {
  //     this.formSubmitCheck = !this.formSubmitCheck;
  //     this.contactForm.reset();
  //   }
  // }

  // formSubmit() {
  //   this.formSubmitCheck = !this.formSubmitCheck;
  // }

}
