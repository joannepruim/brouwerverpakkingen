import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    loadChildren: () =>
        import("./home/home.module").then((m) => m.HomeModule),
  },
  {
    path: "about",
    loadChildren: () =>
        import("./about/about.module").then((m) => m.AboutModule),
  },
  {
    path: "verpakkingen",
    loadChildren: () =>
        import("./diensten/verpakkingen/verpakkingen.module").then((m) => m.VerpakkingenModule),
  },
  {
    path: "slitting",
    loadChildren: () =>
        import("./diensten/slitting/slitting.module").then((m) => m.SlittingModule),
  },
  {
    path: "offerte",
    loadChildren: () =>
        import("./offerte/offerte.module").then((m) => m.OfferteModule),
  },
  {
    path: "offerte-tool",
    loadChildren: () =>
        import("./offerte/offerte-tool/offerte-tool.module").then((m) => m.OfferteToolModule),
  },
  {
    path: "",
    pathMatch: "full",
    redirectTo: "home",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }



