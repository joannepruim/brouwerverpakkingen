import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OfferteToolComponent } from './offerte-tool.component';

const routes: Routes = [{ path: '', component: OfferteToolComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfferteToolRoutingModule { }
