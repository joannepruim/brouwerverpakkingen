import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";

interface SubmitEvent extends Event {
  submitter: HTMLElement;
}

@Component({
  selector: 'app-offerte-tool',
  templateUrl: './offerte-tool.component.html',
  styleUrls: ['./offerte-tool.component.styl']
})
export class OfferteToolComponent implements OnInit {
  isSuccess?: boolean;
  successMessage?: Object;
  errorMessage?: any;
  response?: Object;

  public showTwomm = true;
  public showThreemm = false;
  public minimum150 = true;
  public minimum200 = false;
  public minimum100 = false;

  offerteForm = new FormGroup({
    diameter: new FormControl('', [
    ]),
    wanddikte: new FormControl('', [
    ]),
    lengte: new FormControl('', [
    ]),
    aantal: new FormControl('', [
    ]),
    voornaam: new FormControl('', [
      Validators.required
    ]),
    achternaam: new FormControl('', [
      Validators.required
    ]),
    adres: new FormControl('', [
      Validators.required
    ]),
    postcode: new FormControl('', [
      Validators.required
    ]),
    plaats: new FormControl('', [
      Validators.required
    ]),
    bedrijf: new FormControl('', [
    ]),
    opmerking: new FormControl('', [
    ]),
    mail: new FormControl('', [
      Validators.required
    ]),
    telefoon: new FormControl('', [
      Validators.required,
      Validators.min(10)
    ])
  });

  constructor(private readonly http: HttpClient) { }

  ngOnInit() {
  }

  fiftymm() {
    this.showTwomm = true;
    this.showThreemm = false;
    this.minimum150 = true;
    this.minimum200 = false;
    this.minimum100 = false;
  }

  seventySixmm() {
    this.showTwomm = true;
    this.showThreemm = false;
    this.minimum150 = false;
    this.minimum200 = true;
    this.minimum100 = false;
  }

  ninetymm() {
    this.showTwomm = true;
    this.showThreemm = false;
    this.minimum150 = false;
    this.minimum200 = true;
    this.minimum100 = false;
  }

  oneHundredmm() {
    this.showTwomm = true;
    this.showThreemm = false;
    this.minimum150 = false;
    this.minimum200 = false;
    this.minimum100 = true;
  }

  oneHundredTenmm() {
    this.showTwomm = true;
    this.showThreemm = false;
    this.minimum150 = false;
    this.minimum200 = false;
    this.minimum100 = true;
  }

  oneHundredTwentymm() {
    this.showTwomm = false;
    this.showThreemm = true;
    this.minimum150 = false;
    this.minimum200 = false;
    this.minimum100 = true;
  }

  oneHundredFiftymm() {
    this.showTwomm = false;
    this.showThreemm = true;
    this.minimum150 = false;
    this.minimum200 = false;
    this.minimum100 = true;
  }

  twoHundredmm() {
    this.showTwomm = false;
    this.showThreemm = true;
    this.minimum150 = false;
    this.minimum200 = false;
    this.minimum100 = true;
  }

  onSubmit(e: SubmitEvent) {
    // Prevent default to avoid sending the original HTML form (default browser action)
    e.preventDefault();

    // TODO: add some validation and prevent sending the form if the form still has errors
    if (this.offerteForm.invalid) {
      this.errorMessage = 'fout'
    }
    if (this.offerteForm.valid) {
      this.successMessage = 'lekker'
    }

    // Send the form
    this.http
        .post("https://formcarry.com/s/0c1QxD9Z3HU", this.offerteForm.value, {
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json"
          }
        })
        .subscribe({
          next: (res) => {
            this.response = res;
            this.isSuccess = true;
            this.successMessage = "Verzonden!";
            this.offerteForm.reset();
          },
          error: (e) => {
            this.errorMessage = e;
          }
        });
  }
}
