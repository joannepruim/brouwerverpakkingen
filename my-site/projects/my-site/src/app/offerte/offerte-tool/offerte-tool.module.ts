import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OfferteToolRoutingModule } from './offerte-tool-routing.module';
import { OfferteToolComponent } from './offerte-tool.component';
import { MenuModule } from "../../menu/menu.module";
import { LogoModule } from "../../logo/logo.module";
import { IntroModule } from "../../intro/intro.module";
import {DienstenModule} from "../../diensten/diensten.module";
import {ContactModule} from "../../contact/contact.module";
import {FooterModule} from "../../footer/footer.module";
import {AboutModule} from "../../about/about.module";
import {ImagesModule} from "../../images/images.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    OfferteToolComponent
  ],
  exports: [
    OfferteToolComponent
  ],
  imports: [
    CommonModule,
    OfferteToolRoutingModule,
    MenuModule,
    LogoModule,
    IntroModule,
    DienstenModule,
    ContactModule,
    FooterModule,
    AboutModule,
    ImagesModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class OfferteToolModule { }
