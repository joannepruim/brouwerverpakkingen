import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OfferteRoutingModule } from './offerte-routing.module';
import { OfferteComponent } from './offerte.component';
import { MenuModule } from "../menu/menu.module";
import { LogoModule } from "../logo/logo.module";
import { IntroModule } from "../intro/intro.module";
import {DienstenModule} from "../diensten/diensten.module";
import {ContactModule} from "../contact/contact.module";
import {FooterModule} from "../footer/footer.module";
import {AboutModule} from "../about/about.module";
import {ImagesModule} from "../images/images.module";
import {OfferteToolModule} from "./offerte-tool/offerte-tool.module";

@NgModule({
  declarations: [
    OfferteComponent
  ],
  exports: [
    OfferteComponent
  ],
  imports: [
    CommonModule,
    OfferteRoutingModule,
    MenuModule,
    LogoModule,
    IntroModule,
    DienstenModule,
    ContactModule,
    FooterModule,
    AboutModule,
    ImagesModule,
    OfferteToolModule,
  ]
})
export class OfferteModule { }
