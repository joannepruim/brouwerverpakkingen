import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OfferteComponent } from './offerte.component';

const routes: Routes = [{ path: '', component: OfferteComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfferteRoutingModule { }
